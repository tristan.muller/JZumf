/*
 * Copyright (C) 2019 Tristan Muller (tristan.muller@cirad.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.karakoukie.jzumf;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tristan Muller (tristan.muller@cirad.fr)
 */
public final class JZumf
{
    public static final byte ONE_BYTE_CHAR = 0;
    public static final byte ONE_BYTE_BOOLEAN = 1;
    public static final byte TWO_BYTES_CHAR = 2;
    public static final byte TWO_BYTES_SHORT = 3;
    public static final byte FOUR_BYTES_INT = 4;
    public static final byte FOUR_BYTES_FLOAT = 5;
    public static final byte EIGHT_BYTES_LONG = 6;
    public static final byte EIGHT_BYTES_DOUBLE = 7;
    public static final byte UTF_8 = 8;

    public static final boolean write(final JZumfFile file)
    {
        /* Check if the file is null */
        if (file == null)
        {
            System.err.println("The JZumfFile to write is null");
            return false;
        }

        /* Check if the file have properties */
        if (file.getProperties().isEmpty())
        {
            System.err.println("The JZumfFile to write doesn't have properties");
            return false;
        }

        /* Check if the file have data */
        if (file.getDatas().isEmpty())
        {
            System.err.println("The JZumfFile to write doesn't have values");
            return false;
        }

        try
        {
            /* Make sure the file extension is .zumf */
            String location = file.getLocation();
            if (!location.endsWith(".zumf"))
            {
                location += ".zumf";
            }

            /* Creata file writer and service executor */
            new FileWriter(location).write("");
            final RandomAccessFile writer = new RandomAccessFile(location, "rw");
            final ExecutorService executor = Executors.newSingleThreadExecutor();

            /* Write each data */
            for (JZumfData data : file.getDatas())
            {
                executor.execute(() ->
                {
                    try
                    {
                        writer.write("#d".getBytes());
                        writer.writeByte(data.getType());

                        switch (data.getType())
                        {
                            case ONE_BYTE_CHAR: 
                                writer.writeByte((char) data.getValue());
                                break;
                            case ONE_BYTE_BOOLEAN: 
                                writer.writeBoolean((boolean) data.getValue());
                                break;
                            case TWO_BYTES_CHAR:
                                writer.writeChar((char) data.getValue());
                                break;
                            case TWO_BYTES_SHORT:
                                writer.writeShort((short) data.getValue());
                                break;
                            case FOUR_BYTES_INT:
                                writer.writeInt((int) data.getValue());
                                break;
                            case FOUR_BYTES_FLOAT:
                                writer.writeFloat((float) data.getValue());
                                break;
                            case EIGHT_BYTES_LONG:
                                writer.writeLong((long) data.getValue());
                                break;
                            case EIGHT_BYTES_DOUBLE:
                                writer.writeDouble((double) data.getValue());
                                break;
                            case UTF_8:
                                writer.writeUTF((String) data.getValue());
                                break;
                        }
                        
//                        writer.writeByte('\n');
                    }
                    catch (IOException ex)
                    {
                        Logger.getLogger(JZumf.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            }
            
            /* Write each property */
            for (JZumfProperty property : file.getProperties())
            {
                executor.execute(() ->
                {
                    try {
                        writer.write("#p".getBytes());
                        writer.writeInt(property.getName().getBytes().length);
                        writer.write(property.getName().getBytes());
                        writer.writeInt(property.getLength());

                        for (Integer key : property.getKeys())
                        {
                            writer.writeInt(key);
                        }

                        writer.writeByte('\n');
                    }
                    catch (IOException ex)
                    {
                        Logger.getLogger(JZumf.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            }

            /* Execute all task and wait the end */
            executor.shutdown();
            while (!executor.isTerminated()) {}
            writer.close();

            /* The writing process is complete */
            return true;
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }

        /* Something wrong happened */
        return false;
    }
    
    public static final JZumfFile read(final String location)
    {
        /* Check if the file to read exists */
        if (!new File(location).exists())
        {
            System.err.println("The file " + location + " doesn't exist.");
            return null;
        }
        
        /* Create the readed file */
        final JZumfFile file = new JZumfFile(location);
        
        try
        {
            /* Create a file writer and service executor */
            final RandomAccessFile reader = new RandomAccessFile(location, "r");
            
            /* Read each byte of file */
            while (reader.getFilePointer() < reader.length())
            {
                /* Get the line which begin by # */
                if (reader.readByte() == '#')
                {
                    /* Get the char after the # symbol */
                    final char header = (char) reader.readByte();
                    
                    /* If the header is marked for a data */
                    if (header == 'd')
                    {
                        /* Get the type of data 
                        (using JZumf.ONE_BYTE_CHAR ...) */
                        final byte type = reader.readByte();

                        /* Read the data's value with the appropriate type */
                        switch(type)
                        {
                            case ONE_BYTE_CHAR:
                                file.addValues(new JZumfData(type, reader.readByte()));
                                break;
                            case ONE_BYTE_BOOLEAN:
                                file.addValues(new JZumfData(type, reader.readBoolean()));
                                break;
                            case TWO_BYTES_CHAR:
                                file.addValues(new JZumfData(type, reader.readChar()));
                                break;
                            case TWO_BYTES_SHORT:
                                file.addValues(new JZumfData(type, reader.readShort()));
                                break;
                            case FOUR_BYTES_INT:
                                file.addValues(new JZumfData(type, reader.readInt()));
                                break;
                            case FOUR_BYTES_FLOAT:
                                file.addValues(new JZumfData(type, reader.readFloat()));
                                break;
                            case EIGHT_BYTES_LONG:
                                file.addValues(new JZumfData(type, reader.readLong()));
                                break;
                            case EIGHT_BYTES_DOUBLE:
                                file.addValues(new JZumfData(type, reader.readDouble()));
                                break;
                            case UTF_8:
                                file.addValues(new JZumfData(type, reader.readUTF()));
                                break;
                        }
                    }
                    /* If the header is marked for a property */
                    else if (header == 'p')
                    {
                        /* Get the property's name */
                        String name = "";
                        final int nameLength = reader.readInt();

                        for (int i = 0; i < nameLength; i++)
                        {
                            name += (char) reader.readByte();
                        }

                        /* Create the readed property */
                        final JZumfProperty property = new JZumfProperty(name);

                        /* Read each key */
                        final int keysLength = reader.readInt();

                        for (int i = 0; i < keysLength; i ++)
                        {
                            property.addKeys(reader.readInt());
                        }

                        /* Add the readed property to the readed file */
                        file.addProperties(property);
                    }
                }
            }

            /* The readings process is complete */
            return file;
        }
        catch (IOException | NumberFormatException e)
        {
            e.printStackTrace();
        }
        
        /* Something wrong happened */
        return null;
    }
}
