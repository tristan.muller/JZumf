/*
 * Copyright (C) 2019 Tristan Muller (tristan.muller@cirad.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.karakoukie.jzumf;

/**
 *
 * @author Tristan Muller (tristan.muller@cirad.fr)
 * @param <T> The value type
 */
public final class JZumfData<T>
{
    private final byte m_intType;
    private T m_gncValue;

    public JZumfData(final byte type, final T value)
    {
        this.m_intType = type;
        this.m_gncValue = value;
    }

    public final void setValue(final T value)
    {
        this.m_gncValue = value;
    }

    public final int getType()
    {
        return m_intType;
    }

    public final T getValue()
    {
        return m_gncValue;
    }
}
