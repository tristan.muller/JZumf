/*
 * Copyright (C) 2019 Tristan Muller (tristan.muller@cirad.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.karakoukie.jzumf;

import java.util.Vector;

/**
 *
 * @author Tristan Muller (tristan.muller@cirad.fr)
 */
public final class JZumfFile
{
    private String m_strLocation;
    private final Vector<JZumfProperty> m_objLstProperty;
    private final Vector<JZumfData> m_objLstData;

    public JZumfFile(final String location)
    {
        this.m_strLocation = location;
        this.m_objLstProperty = new Vector();
        this.m_objLstData = new Vector();
    }
    
    public final void addProperties(final JZumfProperty... properties)
    {
        /* Add each specified property */
        for (final JZumfProperty property : properties)
        {
            m_objLstProperty.add(property);
        }
    }
    
    public final void addValues(final JZumfData... values)
    {
        /* Add each specified value */
        for (final JZumfData value : values)
        {
            /* Make sure there are no duplicates */
            for (JZumfData data : m_objLstData)
            {
                if (value.getValue().equals(data.getValue()))
                {
                    return;
                }
            }
            
            /* Add value to the list */
            m_objLstData.add(value);
        }
    }
    
    public final void addProperty(final String name, final byte type, final Object... values)
    {
        final JZumfProperty property = new JZumfProperty(name);
        
        for (Object value : values) {
            JZumfData data = new JZumfData(type, value);
            addValues(data);
            property.addKeys(m_objLstData.indexOf(data));
        }
        
        addProperties(property);
    }
    
    public final void setLocation(final String location)
    {
        this.m_strLocation = location;
    }
    
    public final String getLocation()
    {
        return m_strLocation;
    }
    
    public final Vector<JZumfProperty> getProperties()
    {
        return this.m_objLstProperty;
    }
    
    public final Vector<JZumfData> getDatas()
    {
        return this.m_objLstData;
    }
    
    public final Vector<JZumfData> getPropertyValues(final String name)
    {        
        /* For each property of this file */
        for (final JZumfProperty property : m_objLstProperty)
        {
            /* Search the required property */
            if (property.getName() == null ? name == null : property.getName().equals(name))
            {
                /* Create the value vector */
                final Vector<JZumfData> values = new Vector();
                
                /* For each value's key of the property */
                for (final int key : property.getKeys())
                {
                    /* Add each property value to the value vector */
                    if (m_objLstData.size() > key)
                    {
                        values.add(m_objLstData.get(key));
                    }
                }
                
                /* The property has been founded, so return */
                return values;
            }
        }
        
        /* The property has not been founded, return null */
        return null;
    }
    
}
