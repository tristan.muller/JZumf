/*
 * Copyright (C) 2019 Tristan Muller (tristan.muller@cirad.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.karakoukie.jzumf;

import java.util.Vector;

/**
 *
 * @author Tristan Muller (tristan.muller@cirad.fr)
 */
public final class JZumfProperty
{
    private final String m_strName;
    private final Vector<Integer> m_intLstKeys;
    private int m_intLength;

    public JZumfProperty(final String name)
    {
        this.m_strName = name;
        this.m_intLstKeys = new Vector();
    }
    
    public final void addKeys(final int... keys)
    {
        /* add each specified key */
        for (int key : keys)
        {
            m_intLength++;
            m_intLstKeys.add(key);
        }
    }

    public final String getName()
    {
        return m_strName;
    }
    
    public final Vector<Integer> getKeys()
    {
        return m_intLstKeys;
    }
    
    public final int getLength()
    {
        return m_intLength;
    }
}
