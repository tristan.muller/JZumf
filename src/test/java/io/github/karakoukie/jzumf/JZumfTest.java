/*
 * Copyright (C) 2019 Tristan Muller (tristan.muller@cirad.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.karakoukie.jzumf;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Tristan Muller (tristan.muller@cirad.fr)
 */
public class JZumfTest {
    
    public JZumfTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of write method, of class JZumf.
     */
    @Test
    public void testWrite() {
        System.out.println("write 'test.zumf' file");
        final JZumfFile file = new JZumfFile("test.zumf");
        
        Object[] vertices = new Object[100000];
        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = i % 50 * 0.1f;
        }
        file.addProperty("vertices", JZumf.FOUR_BYTES_FLOAT, vertices);
        
        Object[] normals = new Object[100000];
        for (int i = 0; i < normals.length; i++) {
            normals[i] = (float) i % 5 * 2 - 1;
        }
        file.addProperty("normals", JZumf.FOUR_BYTES_FLOAT, normals);
        
        boolean result = JZumf.write(file);
        assertEquals("JZumf file writing test", true, result);
    }

    /**
     * Test of read method, of class JZumf.
     */
    @Test
    public void testRead() {
        System.out.println("read 'test.zumf' file");
        JZumfFile result = JZumf.read("test.zumf");
        
        System.out.println("write 'read.zumf' file");
        result.setLocation("read.zumf");
        JZumf.write(result);
        
        assertNotEquals("JZumf file reading test", null, result);
    }
    
}
