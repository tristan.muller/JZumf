/*
 * Copyright (C) 2019 Tristan Muller (tristan.muller@cirad.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.karakoukie.jzumf;

import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Base64;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tristan Muller (tristan.muller@cirad.fr)
 */
public class test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try
        {
            new FileWriter("test.txt").write("");
            RandomAccessFile file = new RandomAccessFile("test.txt", "rw");
            
            ExecutorService executor = Executors.newFixedThreadPool(10);
            
            Runnable genuineWorker = new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        file.write("word".getBytes()); // write 4 bytes
                        file.writeByte('\n'); // write 1 byte only
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            };
            
            for (int i = 0; i < 1; i++)
            {
                executor.execute(genuineWorker);
            }
            
            executor.shutdown();
            
            while (!executor.isTerminated()) {}
            
            file.close();
        } 
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
}
